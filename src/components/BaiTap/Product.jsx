import React, { Component } from "react";

export default class Product extends Component {
  render() {
    let { sanPham, xemChiTiet } = this.props;
    return (
      <div className="col-xs-12 col-sm-6 col-md-6 col-lg-3">
        <div className="container">
          <div className="card bg-light">
            <img
              className="card-img-top"
              src={sanPham.hinhAnh}
              height={310}
              alt="Title"
            />
            <div className="card-body">
              <h4 className="card-title">{sanPham.tenSP}</h4>
              <p className="card-text">{sanPham.gia}</p>
              <a
                href="#"
                className="btn btn-primary"
                data-bs-toggle="modal"
                data-bs-target="#modalId"
                onClick={() => xemChiTiet(sanPham)}
              >
                Detail
              </a>
              <a href="#" className="btn btn-danger">
                Cart
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }

  /* render() {
    return (
      <div className="col-xs-12 col-sm-6 col-md-6 col-lg-3">
        <div className="container">
          <div className="card bg-light">
            <img
              className="card-img-top"
              src="./img/sp_iphone14.jpg"
              alt="Title"
            />
            <div className="card-body">
              <h4 className="card-title">iPhone 14</h4>
              <p className="card-text">
                iPhone 14 features a new all-screen design. Face ID, which makes
                your face your password
              </p>
              <a href="#" className="btn btn-primary">
                Detail
              </a>
              <a href="#" className="btn btn-danger">
                Cart
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  } */
}
