import React, { Component } from "react";
import { connect } from "react-redux";

class FormSinhVien extends Component {
  state = {
    values: {
      maSV: "",
      hoTen: "",
      email: "",
      soDienThoai: "",
    },
    errors: {
      maSV: "",
      hoTen: "",
      email: "",
      soDienThoai: "",
    },
  };

  handleChange = (e) => {
    // Lấy giá trị mỗi lần value input thay đổi bởi người dùng
    let tagInput = e.target;
    let { name, value, type, pattern } = tagInput;

    let errorMessage = "";

    // Kiểm tra bất kỳ control input nào người dùng nhập vào đều kiểm tra rỗng
    if (value.trim() == "") {
      errorMessage = name + " không được bỏ trống!";
    }

    // Kiểm tra email
    if (type == "email") {
      const regex = new RegExp(pattern, "g");
      if (!regex.test(value)) {
        errorMessage = name + " không đúng định dạng!";
      }
    }
    if (name == "soDienThoai") {
      const regex = new RegExp(pattern, "g");
      if (!regex.test(value)) {
        errorMessage = name + " không đúng định dạng!";
      }
    }

    // Cập nhật giá trị values cho state
    let values = { ...this.state.values, [name]: value };
    // Cập nhật lỗi cho state
    let errors = { ...this.state.errors, [name]: errorMessage };

    this.setState(
      {
        values: values,
        errors: errors,
      },
      () => {
        console.log(this.state);
      }
    );
  };

  handleSubmit = (e) => {
    e.preventDefault(); // Cản sự kiện submit reload trang của browser
    this.props.themSinhVien(this.state.values);
  };

  renderButtonSubmit = () => {
    let isDisabled = false;
    for (let key in this.state.values) {
      if (this.state.values[key] == "" || this.state.errors[key] != "") {
        isDisabled = true;
        break;
      }
    }
    /* Object.entries(this.state.values).forEach(([_, value]) => {
      if (value == "") isDisabled = true;
    });
    Object.entries(this.state.errors).forEach(([_, error]) => {
      if (error != "") isDisabled = true;
    }); */
    return (
      <button type="submit" className="btn btn-success" disabled={isDisabled}>
        Thêm sinh viên
      </button>
    );
  };

  render() {
    return (
      <div className="container">
        <div className="card text-start">
          <div className="card-header bg-dark text-white">
            <h3>Thông tin sinh viên</h3>
          </div>
          <div className="card-body">
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="col-6">
                  <label className="form-label">Mã SV</label>
                  <input
                    type="text"
                    className="form-control"
                    name="maSV"
                    value={this.state.values.maSV}
                    onChange={this.handleChange}
                  />
                  <p className="text-danger">{this.state.errors.maSV}</p>
                </div>
                <div className="col-6">
                  <label className="form-label">Họ tên</label>
                  <input
                    type="text"
                    className="form-control"
                    name="hoTen"
                    value={this.state.values.hoTen}
                    onChange={this.handleChange}
                  />
                  <p className="text-danger">{this.state.errors.hoTen}</p>
                </div>
              </div>
              <div className="row mt-2">
                <div className="col-6">
                  <label className="form-label">Email</label>
                  <input
                    type="email"
                    pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                    className="form-control"
                    name="email"
                    value={this.state.values.email}
                    onChange={this.handleChange}
                  />
                  <p className="text-danger">{this.state.errors.email}</p>
                </div>
                <div className="col-6">
                  <label className="form-label">Số điện thoại</label>
                  <input
                    type="text"
                    pattern="^(0|[1-9][0-9]*)$"
                    className="form-control"
                    name="soDienThoai"
                    value={this.state.values.soDienThoai}
                    onChange={this.handleChange}
                  />
                  <p className="text-danger">{this.state.errors.soDienThoai}</p>
                </div>
              </div>
              <div className="row mt-2">
                <div className="col-md-12 text-end">
                  {this.renderButtonSubmit()}
                  {/* <button
                    type="submit"
                    className="btn btn-success"
                    disabled={this.state.isDisabled}
                  >
                    Thêm sinh viên
                  </button> */}
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    themSinhVien: (sinhVien) => {
      const action = {
        type: "THEM_SINH_VIEN",
        sinhVien,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(FormSinhVien);
