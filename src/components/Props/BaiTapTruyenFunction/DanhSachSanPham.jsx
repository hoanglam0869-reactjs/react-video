import React, { Component } from "react";
import phoneData from "../../../data/phoneData.json";
import SanPham from "./SanPham";

export default class DanhSachSanPham extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sanPhamChiTiet: phoneData[0],
    };
  }

  renderSanPham = () => {
    return phoneData.map((dt, index) => (
      <SanPham key={index} dt={dt} xemChiTiet={this.xemChiTiet} />
    ));
  };

  // Dữ liệu cần lấy ở component DanhSachSanPham => đặt callback function tại DanhSachSanPham
  // Xử lý nút xem chi tiết
  xemChiTiet = (sanPham) => {
    // Thay đổi giá trị phone xemChiTiet
    this.setState({
      sanPhamChiTiet: sanPham,
    });
  };

  render() {
    let { sanPhamChiTiet } = this.state;
    return (
      <div className="container">
        <div className="row">{this.renderSanPham()}</div>
        <hr />
        <div className="row">
          <div className="col-6 text-start">
            <h3>{sanPhamChiTiet.tenSP}</h3>
            <img src={sanPhamChiTiet.hinhAnh} width={250} height={300} alt="" />
          </div>
          <div className="col-6">
            <h3>Thông số kỹ thuật</h3>
            <table className="table">
              <tr>
                <td>Màn hình</td>
                <td>{sanPhamChiTiet.manHinh}</td>
              </tr>
              <tr>
                <td>Hệ điều hành</td>
                <td>{sanPhamChiTiet.heDieuHanh}</td>
              </tr>
              <tr>
                <td>Camera trước</td>
                <td>{sanPhamChiTiet.cameraTruoc}</td>
              </tr>
              <tr>
                <td>Camera sau</td>
                <td>{sanPhamChiTiet.cameraSau}</td>
              </tr>
              <tr>
                <td>RAM</td>
                <td>{sanPhamChiTiet.ram}</td>
              </tr>
              <tr>
                <td>ROM</td>
                <td>{sanPhamChiTiet.rom}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
