import React, { Component } from "react";

export default class SanPham extends Component {
  render() {
    let { dt, xemChiTiet } = this.props;
    return (
      <div className="col-4">
        <div className="card">
          <img
            className="card-img-top"
            src={dt.hinhAnh}
            height={300}
            alt="Title"
          />
          <div className="card-body">
            <h4 className="card-title">{dt.tenSP}</h4>
            <button onClick={() => xemChiTiet(dt)} className="btn btn-success">
              Xem chi tiết
            </button>
          </div>
        </div>
      </div>
    );
  }
}
