import React, { Component } from "react";

export default class ModalGioHang extends Component {
  render() {
    // Lấy dữ liệu gioHang từ bài tập giỏ hàng
    const { gioHang, xoaGioHang, tangGiamSoLuong } = this.props;
    /* Modal Body */
    /* if you want to close by clicking outside the modal, delete the last endpoint:data-bs-backdrop and data-bs-keyboard */
    return (
      <div
        className="modal fade"
        id="modalId"
        tabIndex={-1}
        data-bs-backdrop="static"
        data-bs-keyboard="false"
        role="dialog"
        aria-labelledby="modalTitleId"
        aria-hidden="true"
      >
        <div
          className="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="modalTitleId">
                Giỏ hàng
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              />
            </div>
            <div className="modal-body">
              <table className="table">
                <thead>
                  <tr>
                    <td>Mã SP</td>
                    <td>Hình ảnh</td>
                    <td>Tên SP</td>
                    <td>Số lượng</td>
                    <td>Đơn giá</td>
                    <td>Thành tiền</td>
                    <td></td>
                  </tr>
                </thead>
                <tbody>
                  {gioHang.map((spGH, index) => (
                    <tr key={index}>
                      <td>{spGH.maSP}</td>
                      <td>
                        <img src={spGH.hinhAnh} alt="" width={50} height={75} />
                      </td>
                      <td>{spGH.tenSP}</td>
                      <td>
                        <button
                          onClick={() => tangGiamSoLuong(spGH.maSP, false)}
                          className="btn btn-primary"
                        >
                          -
                        </button>
                        {spGH.soLuong}
                        <button
                          onClick={() => tangGiamSoLuong(spGH.maSP, true)}
                          className="btn btn-primary"
                        >
                          +
                        </button>
                      </td>
                      <td>{spGH.giaBan.toLocaleString()}</td>
                      <td>{(spGH.soLuong * spGH.giaBan).toLocaleString()}</td>
                      <td>
                        <button
                          onClick={() => xoaGioHang(spGH.maSP)}
                          className="btn btn-danger"
                        >
                          Xóa
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
                <tfoot>
                  <tr>
                    <td colSpan="5"></td>
                    <td>Tổng tiền</td>
                    <td>
                      {gioHang
                        .reduce((tongTien, spGioHang, index) => {
                          return (tongTien +=
                            spGioHang.giaBan * spGioHang.soLuong);
                        }, 0)
                        .toLocaleString()}
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-bs-dismiss="modal"
              >
                Close
              </button>
              <button type="button" className="btn btn-primary">
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }

  /*   <!-- Modal trigger button -->
  <button type="button" class="btn btn-primary btn-lg" data-bs-toggle="modal" data-bs-target="#modalId">
    Launch
  </button>
  <!-- Optional: Place to the bottom of scripts -->
  <script>
    const myModal = new bootstrap.Modal(document.getElementById('modalId'), options)

  </script> */
}
