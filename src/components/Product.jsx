import React, { Component } from "react";

export default class Product extends Component {
  // Thuộc tính

  // Phương thức
  render() {
    // Nội dung thẻ được định nghĩa trong hàm render
    return (
      <div className="card text-white bg-dark">
        <img
          className="card-img-top"
          src="https://www.dairyfoods.com/ext/resources/DF/2006/09/Files/Images/dfx0906-npr-img2c.jpg"
          alt="Title"
          style={{ width: 100, height: 180 }}
        />
        <div className="card-body">
          <h4 className="card-title">Title</h4>
          <p className="card-text">Text</p>
        </div>
      </div>
    );
  }
}
