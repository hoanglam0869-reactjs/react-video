import React, { Component } from "react";

export default class DemoIf extends Component {
  constructor(props) {
    super(props);
    // this.state là thuộc tính có sẵn của component, chứa các thuộc tính có khả năng thay đổi bởi 1 sự kiện nào đó của component
    this.state = {
      // Thuộc tính
      isLogin: false,
      username: "",
    };
  }

  login = () => {
    // this.setState(): là phương thức kế thừa từ class component => thay đổi giá trị state và gọi hàm render => render lại giao diện
    // setState là phương thức bất đồng bộ
    this.setState(
      {
        isLogin: true,
        username: "Lâm CyberSoft",
      },
      () => console.log(this.state)
    );
  };

  logout = () => {
    this.setState(
      {
        isLogin: false,
        username: "",
      },
      () => console.log(this.state)
    );
  };

  // Cách 1: Dùng phương thức kết hợp if để xác định nội dung render ra giao diện
  renderContent = () => {
    if (this.state.isLogin) {
      // isLogin === true => Người dùng đã đăng nhập
      this.state.username = "Lâm";
      return (
        <div>
          Hello {this.state.username}! CyberSoft <button>Logout</button>
        </div>
      );
    }
    return (
      <div>
        <button>Login</button>
      </div>
    );
  };

  // Phương thức render của component DemoIf
  render() {
    return (
      <div>
        {/* {this.renderContent()} */}
        {this.state.isLogin ? (
          <div>
            Hello {this.state.username}! CyberSoft{" "}
            <button onClick={this.logout}>Logout</button>
          </div>
        ) : (
          <div>
            <button onClick={this.login}>Login</button>
          </div>
        )}
      </div>
    );
  }
}
