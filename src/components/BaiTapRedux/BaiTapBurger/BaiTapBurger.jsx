import React, { Component } from "react";
import Burger from "./Burger";
import Menu from "./Menu";

export default class BaiTapBurger extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-6">
            <Burger />
          </div>
          <div className="col-6">
            <Menu />
          </div>
        </div>
      </div>
    );
  }
}
