import React, { Component } from "react";
import "./BaiTapBurger.css";
import { connect } from "react-redux";

class Burger extends Component {
  renderFood = () => {
    let salad = this.props.burgerState.burger.salad;
    let saladArr = Array.from(Array(salad), (_, index) => (
      <div className="salad" key={index}></div>
    ));
    let cheese = this.props.burgerState.burger.cheese;
    let cheeseArr = Array.from(Array(cheese), (_, index) => (
      <div className="cheese" key={index}></div>
    ));
    let beef = this.props.burgerState.burger.beef;
    let beefArr = Array.from(Array(beef), (_, index) => (
      <div className="beef" key={index}></div>
    ));
    return [saladArr, cheeseArr, beefArr];
  };
  render() {
    console.log(this.props.burgerState);
    return (
      <div>
        <h1>Cửa hàng burger cybersoft</h1>
        <div className="breadTop"></div>
        <p>Chọn thức ăn</p>
        {this.renderFood()}
        <div className="breadBottom"></div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    burgerState: state.BurgerReducer,
  };
};

export default connect(mapStateToProps, null)(Burger);
