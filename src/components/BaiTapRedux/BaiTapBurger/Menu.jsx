import React, { Component } from "react";
import { connect } from "react-redux";

class Menu extends Component {
  render() {
    const { burger, menu, total } = this.props.burgerState;
    return (
      <div>
        <h1>Chọn thức ăn</h1>
        <table className="table">
          <thead>
            <tr>
              <th>Thức ăn</th>
              <th></th>
              <th>Giá</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>salad</td>
              <td>
                <button
                  onClick={() => this.props.orderFood("salad", -1)}
                  className="btn btn-danger"
                >
                  -
                </button>
                {burger.salad}
                <button
                  onClick={() => this.props.orderFood("salad", 1)}
                  className="btn btn-success"
                >
                  +
                </button>
              </td>
              <td>{burger.salad * menu.salad}</td>
            </tr>
            <tr>
              <td>cheese</td>
              <td>
                <button
                  onClick={() => this.props.orderFood("cheese", -1)}
                  className="btn btn-danger"
                >
                  -
                </button>
                {burger.cheese}
                <button
                  onClick={() => this.props.orderFood("cheese", 1)}
                  className="btn btn-success"
                >
                  +
                </button>
              </td>
              <td>{burger.cheese * menu.cheese}</td>
            </tr>
            <tr>
              <td>beef</td>
              <td>
                <button
                  onClick={() => this.props.orderFood("beef", -1)}
                  className="btn btn-danger"
                >
                  -
                </button>
                {burger.beef}
                <button
                  onClick={() => this.props.orderFood("beef", 1)}
                  className="btn btn-success"
                >
                  +
                </button>
              </td>
              <td>{burger.beef * menu.beef}</td>
            </tr>
          </tbody>
        </table>
        <div className="text-end">
          <h5>Tổng tiền {total}</h5>
          <button className="btn btn-primary">Thanh toán</button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    burgerState: state.BurgerReducer,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    orderFood: (food, amount) => {
      const action = {
        type: "ORDER_FOOD",
        food,
        amount,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
