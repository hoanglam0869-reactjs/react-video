import React, { Component } from "react";

// Kết nối redux (connect hàm kết nối reactComponent - reduxStore)
import { connect } from "react-redux";

class ModalGioHangRedux extends Component {
  renderGioHang = () => {
    // this.props.gioHang là thuộc tính nhận từ redux
    return this.props.gioHang.map((spGH, index) => (
      <tr key={index}>
        <td>{spGH.maSP}</td>
        <td>{spGH.tenSP}</td>
        <td>
          <img src={spGH.hinhAnh} alt="" width={50} height={75} />
        </td>
        <td>{spGH.giaBan}</td>
        <td>
          <button onClick={() => this.props.tangGiamSoLuong(index, false)}>
            -
          </button>
          {spGH.soLuong}
          <button onClick={() => this.props.tangGiamSoLuong(index, true)}>
            +
          </button>
        </td>
        <td>{spGH.giaBan * spGH.soLuong}</td>
        <td>
          <button
            onClick={() => this.props.xoaGioHangIndex(index)}
            className="btn btn-danger"
          >
            Xóa
          </button>
          <button
            onClick={() => this.props.xoaGioHangMaSP(spGH.maSP)}
            className="btn btn-danger"
          >
            Xóa Mã SP
          </button>
        </td>
      </tr>
    ));
  };

  render() {
    return (
      <div className="container">
        <table className="table">
          <thead>
            <tr>
              <th>Mã sp</th>
              <th>Tên sp</th>
              <th>Hình ảnh</th>
              <th>Giá bán</th>
              <th>Số lượng</th>
              <th>Thành tiền</th>
              <th></th>
            </tr>
          </thead>
          <tbody>{this.renderGioHang()}</tbody>
          <tfoot>
            <tr>
              <td colSpan="5"></td>
              <td>
                {this.props.gioHang.reduce((tongTien, spGioHang, index) => {
                  return (tongTien += spGioHang.soLuong * spGioHang.giaBan);
                }, 0)}
              </td>
            </tr>
          </tfoot>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  // state: là store tổng, => truy xuất đến GioHangReducer => biến state trên GioHangReducer
  return {
    // => tạo ra 1 props của component ModalGioHangRedux
    gioHang: state.GioHangReducer.gioHang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    xoaGioHangIndex: (index) => {
      const action = {
        type: "XOA_GI0_HANG",
        index: index,
      };
      // Đưa action lên reducer
      dispatch(action);
    },
    xoaGioHangMaSP: (maSP) => {
      const action = {
        type: "XOA_GIO_HANG_MSP",
        maSP: maSP,
      };
      dispatch(action);
    },
    tangGiamSoLuong: (index, tangGiam) => {
      const action = {
        type: "TANG_GIAM_SL",
        index,
        tangGiam,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalGioHangRedux);
