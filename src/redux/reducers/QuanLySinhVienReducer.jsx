const stateDefault = {
  mangSinhVien: [
    {
      maSV: 1,
      hoTen: "Nguyễn Văn A",
      soDienThoai: "0909090909",
      email: "abc@gmail.com",
    },
    {
      maSV: 2,
      hoTen: "Nguyễn Văn A",
      soDienThoai: "0909090909",
      email: "abc@gmail.com",
    },
  ],
};

export const QuanLySinhVienReducer = (state = stateDefault, action) => {
  switch (action.type) {
    case "THEM_SINH_VIEN": {
      let mangSVUpdate = [...state.mangSinhVien, action.sinhVien];
      state.mangSinhVien = mangSVUpdate;
      return { ...state };
    }
    default: {
      return { ...state };
    }
  }
};
