import { combineReducers } from "redux";
import { GioHangReducer } from "./GioHangReducer";
import { BurgerReducer } from "./BurgerReducer";
import { QuanLySinhVienReducer } from "./QuanLySinhVienReducer";

// store tổng ứng dụng
export const rootReducer = combineReducers({
  // Nơi sẽ chứa các reducer cho nghiệp vụ (store con)
  GioHangReducer,
  BurgerReducer,
  QuanLySinhVienReducer,
});
