// Khởi tạo giá trị ban đầu của store
const stateGioHang = {
  gioHang: [
    {
      maSP: 1,
      tenSP: "iPhone",
      hinhAnh: "./img/vsphone.jpg",
      soLuong: 1,
      giaBan: 1000,
      thanhTien: 1000,
    },
  ],
};

export const GioHangReducer = (state = stateGioHang, action) => {
  switch (action.type) {
    case "THEM_GIO_HANG": {
      // Xử lý logic thêm giỏ hàng
      let gioHangCapNhat = [...state.gioHang];
      let index = gioHangCapNhat.findIndex(
        (spGH) => spGH.maSP == action.spGioHang.maSP
      );
      if (index == -1) {
        gioHangCapNhat.push(action.spGioHang);
      } else {
        gioHangCapNhat[index].soLuong += 1;
      }
      state.gioHang = gioHangCapNhat;
      return { ...state };
    }
    case "XOA_GI0_HANG": {
      let gioHangCapNhat = [...state.gioHang];
      // Xóa giỏ hàng dựa vào index
      gioHangCapNhat.splice(action.index, 1);
      // Gán giỏ hàng mới cho state.gioHang => render lại giao diện
      state.gioHang = gioHangCapNhat;
      return { ...state };
    }
    case "XOA_GIO_HANG_MSP": {
      let gioHangCapNhat = [...state.gioHang];

      // Tìm index sp dựa vào mã sp
      let index = gioHangCapNhat.findIndex((spGH) => spGH.maSP == action.maSP);
      if (index != -1) {
        // Xóa giỏ hàng dựa vào index
        gioHangCapNhat.splice(index, 1);
      }
      // Gán giỏ hàng mới cho state.gioHang => render lại giao diện
      state.gioHang = gioHangCapNhat;
      return { ...state };
    }
    case "TANG_GIAM_SL": {
      const { index, tangGiam } = action;
      // Xử lý tăng giảm số lượng
      let gioHangCapNhat = [...state.gioHang];
      if (tangGiam) {
        gioHangCapNhat[index].soLuong += 1;
      } else if (gioHangCapNhat[index].soLuong > 1) {
        gioHangCapNhat[index].soLuong -= 1;
      }
      // Cập nhật lại state của GioHangReducer
      state.gioHang = gioHangCapNhat;
      return { ...state };
    }
    default: {
      return { ...state };
    }
  }
};
