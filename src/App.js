import logo from "./logo.svg";
import "./App.css";
import Header from "./components/Header";
import Product from "./components/Product";
import BaiTapComponent from "./components/BaiTap/BaiTapComponent";
import SinhVien from "./components/DataBinding/SinhVien";
import Event from "./components/DataBinding/Event";
import DemoIf from "./components/CauTrucDieuKhien_Rerender/DemoIf";
import BaiTapState from "./components/CauTrucDieuKhien_Rerender/BaiTapState";
import DemoVongLap from "./components/CauTrucLap/DemoVongLap";
import BaiTapVongLap from "./components/BaiTap2/BaiTapVongLap";
import DemoProps from "./components/Props/DemoProps";
import BaiTapTruyenFunction from "./components/Props/BaiTapTruyenFunction/BaiTapTruyenFunction";
import BaiTapGioHang from "./components/Props/BaiTapGioHang/BaiTapGioHang";
import BTGioHangRedux from "./components/BaiTapRedux/BaiTapGioHang/BTGioHangRedux";
import BaiTapBurger from "./components/Props/BaiTapBurger/BaiTapBurger";
import BaiTapForm from "./components/BaiTapForm/BaiTapForm";
// import BaiTapBurger from "./components/BaiTapRedux/BaiTapBurger/BaiTapBurger";

function App() {
  return (
    <div className="App">
      {/* <Header />
      <div className="container">
        <Product />
      </div> */}
      {/* <BaiTapComponent /> */}
      {/* <SinhVien /> */}
      {/* <Event /> */}
      {/* <DemoIf /> */}
      {/* <BaiTapState /> */}
      {/* <DemoVongLap /> */}
      {/* <BaiTapVongLap /> */}
      {/* <DemoProps title={"Cybersoft"} /> */}
      {/* <BaiTapTruyenFunction /> */}
      {/* <BaiTapGioHang /> */}
      {/* <BTGioHangRedux /> */}
      {/* <BaiTapBurger /> */}
      <BaiTapForm />
    </div>
  );
}

export default App;
